provider "aws" {
    version = "~>3.0"
    region = "us-east-1"
  
}

resource "aws_rds_cluster" "example_db_clustername" { # write your DB cluster name
  cluster_identifier = var.rds_cluster_identifier
  engine = var.rds_engine
  availability_zones = var.rds_availability_zones
  database_name = var.rds_database_name
  master_username = var.rds_master_username
  master_password = var.rds_master_password
  backup_retention_period = var.rds_backup_retention_period
  preferred_backup_window = var.rds_preferred_backup_window
  skip_final_snapshot = var.skip_final_snapshot

}

resource "aws_rds_cluster_instance" "example_db_cluster_instance" {
    count = 1
    identifier = var.rds_cluster_identifier
    cluster_identifier = aws_rds_cluster.<enter your cluster identifier>.id
    instance_instance_class = var.rds_instance_type
    engine = aws_rds_cluster.<enter your rds clueter>
    engine_version = aws_rds_cluster.<rds_engine_version>


  
}