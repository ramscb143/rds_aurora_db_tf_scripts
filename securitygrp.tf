resource "aws_security_group" "enter your rds security group" {
    name = "enter your security group"
    description = "RDS Security group"

    ingress {
        from_port = 5432
        to_port = 5432
        protocol = "tcp"
        cidr_blocks = [ "enter DB cider values" ]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "tcp"
        cidr_blocks = [ "0.0.0.0/0" ]
    }

    tags = {
      "Name" = "Enter your tag value"
    }
  
}