# A list of EC2 Availability Zones for the DB cluster storage.
variable "rds_availability_zones" {
    default = ["us-east-1a", "us-east-1b"]
      
}

# The traget backtrack window, in seconds. To disable backtracking, set this value to 0.
variable "rds_backtrack_window" {
    default = "0"
  
}

# The days to retain backups for, Default 1
variable "rds_backup_retention_period" {
    default = "1"
}

# Creates a unique cluster identifier beginning with the specified prefix.
variable "rds_cluster_identifier" {
    default = "write your cluster name" # RK
  
}


#copy all Cluster tages to snapshots. Default is false.
variable "rds_copy_tags_to_snapshot" {
    default = "false"
  
}

# Name for an automatically created database on cluster creation
variable "rds_database_name" {
    default = "give your database nmae"  # RK
  
}

# A cluster parameter group to associate with the cluster.
variable "rds_db_cluster_parameter_group_name" {
    default = "give your db parameter group name" # RK
  
}


#If the DB instance should have deletion protection enabled
variable "rds_deletion_protection" {
    default = "true"
  
}

# List of log types to export to cloudwatch.supported: audit, error, slowquery, postgresql
variable "rds_enabled_cloudwatch_logs_exports" {
    default = "enabled"
  
}

# The database engine mode. Valid values: gobal, multimaster, parallelquery, provisioned, serverless. Defaults to:provisioned
variable "rds_engine_mode" {
    default = "provisioned"
  
}

# The database engine version. Updating this agrument results in an outage
variable "rds_engine_version" {
    default = "11.9, change your version"  # RK
  
}

# The name of the database engine to be used for this DB cluster. Defaults to aurora. Valid values: aurora, aurora-mysql, aurora-postgresql
variable "rds_engine" {
    default = "aurora-postgresql"
  
}

# The global cluster identifier specified on aws_rds_global_cluster.
variable "rds_global_cluster_identifier" {
    default = "provisioned"
  
}

# Specifies wheteher or mappings of AWS Identity and Access Management (IAM accounts to database accounts is enabled.)
variable "rds_iam_database_authentication_enabled" {
    default = "provisioned"
  
}

# A list of ARNs for the IAM roles to associate to the RDS cluster.
variable "rds_iam_roles" {
    default = "provisioned"
  
}

#The ARN for the KMS encryption key. When specifying kms_key_id, storage_encrypted needs to be set to true.
variable "rds_kms_key_id" {
    default = "provisioned"
  
}

# Password for the master DB user
variable "rds_master_password" {
    default = "enter DB master password"  #RK
  
}

# Username for the master DB user
variable "rds_master_username" {
    default = "enter your DB master username"
  
}

# The port on which the DB accepts connections
variable "rds_port" {
    default = "provisioned"
  
}

# The daily time rand during which automated backups are created if automated backups are enabled using the BackupRetentionPeriod parameter.
variable "rds_preferred_backup_window" {
    default = "07:00-09:00"
  
}

# the weekly time range during which system maintenance can occur, in UTC
variable "rds_preferred_maintenance_window" {
    default = "provisioned"
  
}

# Determines whether a final DB snapshot is created before the DB cluster is deleted
variable "rds_skip_final_snapshot" {
    default = "true"
  
}

# Spefified whether the DB cluster is encrypted. The default is false for provisioned engine_mode and true for serverless engine_mode.
variable "rds_storage_encrypted" {
    default = "provisioned"
  
}

#Specifies RDS Instance type
variable "rds_instance_type" {
    default = "db.r5.large" # RK change your rds instance based on your requirement
  
}

# Specifies RDS storage size
variable "rds_storage_size" {
    default = "10" # RK, change your DB storage size basesd on your requirement 
  
}


#Specifies if RDS will be accessiable
variable "rds_publiclyPaccessible" {
    default = "true"  # RK change your based on requirement
  
}

# Specifies if RDS will skip final snapshot check before destorying
variable "skip_final_snapshot" {
    default = "true"
  
}

#Specifies terraform state bucket to save terraform state data
variable "dbtfstatebucket" {
  default = "db-tf-state" #RK change your details based on requirment
}

# The S3 bucket name for storing terraform state.
variable "tfstate_bucket" {
    description = "Bucket name for your application zip folders"
    type = string
    default = "tfstate-bucketeastname" # change your tf bucketname
  
}

