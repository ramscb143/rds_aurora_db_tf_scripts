terraform {
  backend "s3" {
      region = "us-east-1"
      bucket = "var.tfstate_bucket"
      key = "var.tfstate_bucket/var.databasetfbucket/state.tfstate" # Change your S3 bucket name here
      encrypt = true
  }
}